# OpenML dataset: breast-cancer-dropped-missing-attributes-values

https://www.openml.org/d/23499

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Smite Chow  
**Source**: http://www.openml.org/d/13 - Date 11 July 1988  
**Please cite**:   

Citation Request: This breast cancer domain was obtained from the University Medical Centre, Institute of Oncology, Ljubljana, Yugoslavia. Thanks go to M. Zwitter and M. Soklic for providing the data. Please include this citation if you plan to use this database.

 1. Title: 

 2. Sources: http://www.openml.org/d/13

 3. Past Usage: Smite Chow

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/23499) of an [OpenML dataset](https://www.openml.org/d/23499). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/23499/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/23499/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/23499/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

